<form action="">

<div class="header" style="border-bottom:1px solid black; padding-bottom:15px" >
        <a class="headname">Product List</a>
        <button type="button" class="btn btn-success btn-md" style="float:right">MASS DELETE</button>
        <button type="button"  style="float:right; margin-right: 15px"><a href="add.php" class="add">ADD</a></button>
        <br>
</div>

<?php
if (! empty($result)) {
    foreach ($result as $k => $v) {
      $typevalue = "";
      if($result[$k]['producttype']=='dvd'){
        $typevalue = $result[$k]['dvd'];
        $typein = "MB";
      }
      if($result[$k]['producttype']=='book'){
        $typevalue = $result[$k]['book'];
        $typein = "KG";
      }
      if($result[$k]['producttype']=='furniture'){
        $typevalue = $result[$k]['furniture'];
        $typein = "";
      }
?>


      <div class="product">
      <input type='checkbox' name="delete-id[]" class='delete-checkbox' value='<?php echo $result[$k]["id"]; ?>'>
      <center>
        <div class="cat" id="sku"><?php echo $result[$k]["SKU"]; ?></div>
        <div class="cat" id="name"><?php echo $result[$k]["name"]; ?></div>
        <div class="cat" id="price"><?php echo $result[$k]["price"] . '$' ; ?></div>
        <div class="cat" id="furniture"><?php echo $result[$k]['producttype'] . ' : ' .  $typevalue . ' ' . $typein; ?></div>
      </center>
      </div>

    
<?php
    }
}
?>

</form>

<footer class="footer">
  <br>
  <center>Scandiweb Test Assigment </center>
  <br>
</footer>


<script type="text/javascript">
    $(document).ready(function(){
        // fetch data from table without reload/refresh page
        loadData();
        function loadData(query){
          $.ajax({
            url : "index.php",
            type: "POST",
            chache :false,
            data:{query:query},
            success:function(response){
              $(".result").html(response);
            }
          });  
        }
        // Delete multiple record 
        $(".btn-success").click(function(){
          
          var id = [];
          $(".delete-checkbox:checked").each(function(){
              id.push($(this).val());
              element = this;
          });
          
          if (id.length > 0) {
                $.ajax({
                    url : "delete.php",
                    type: "POST",
                    cache:false,
                    data:{deleteId:id},
                    success:function(response){
                      if (response==1) {
                        window.location = window.location
                      }else{
                          alert("Somethins went wrong");
                      }
                    }
                });

          }
        });
    });
</script>

